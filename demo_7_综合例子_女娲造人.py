from demo_7_综合例子_人类模板 import RenLei,HanGuoRen,YinDuRen

# 开始从航国人类(HanGuoRen)创建航国人具体的对象，两个:si_mi_da_1 ， si_mi_da_2
si_mi_da_1 = HanGuoRen("思密达1",18)
# 调用chi_fan方法
si_mi_da_1.chi_fan("烤五花肉")

print("%s的皮肤颜色：%s\n" % (si_mi_da_1.xing_ming,si_mi_da_1.pi_fu))
print("%s晒了一会日光浴\n" % si_mi_da_1.xing_ming)
si_mi_da_1.pi_fu = "被晒黑了"
print("%s的皮肤颜色：%s\n" % (si_mi_da_1.xing_ming,si_mi_da_1.pi_fu))


si_mi_da_2 = HanGuoRen("思密达2",51)
si_mi_da_2.chi_fan("海鲜大酱汤")
si_mi_da_2.shui_jiao
print("%s的皮肤颜色：%s\n" % (si_mi_da_2.xing_ming,si_mi_da_2.pi_fu))
# print("泡菜秘方：%s" % HanGuoRen.__pao_cai_mi_fang)
print("泡菜秘方：%s" % si_mi_da_2.kan_kan_pao_cai_mi_fang())

print("航国人有%d个眼睛，%d个鼻子\n" % (HanGuoRen.yan_jing,HanGuoRen.bi_zi))



# 开始从银都人类(YinDuRen)创建银都人具体的对象：yin_du_ren_1
yin_du_ren_1 = YinDuRen("阿三1号", 21)
yin_du_ren_1.chi_fan("咖喱牛腩")

print("%s的皮肤颜色：%s\n" % (yin_du_ren_1.xing_ming,yin_du_ren_1.pi_fu))
print("%s去恒河洗了一下\n" % yin_du_ren_1.xing_ming)
yin_du_ren_1.pi_fu = "白了一点"
print("%s的皮肤颜色：%s\n" % (yin_du_ren_1.xing_ming,yin_du_ren_1.pi_fu))

yin_du_ren_1.shi_wu = "小鸡炖蘑菇"
print("点的食物：%s\n" % yin_du_ren_1.__shi_wu)
yin_du_ren_1.shi_wu = "咖喱鱼头"
yin_du_ren_1.chi_fan(yin_du_ren_1.shi_wu)
