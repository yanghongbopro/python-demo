class Swan:
    """天鹅类"""
    __neck_swan = '天鹅第脖子很长'
    neck_swan = '没有下划线的'
    def __init__(self):
        print("__init__():", Swan.__neck_swan)


swan = Swan()
print("加入类名：", Swan._Swan__neck_swan)
print("不加类名：", Swan.__neck_swan)
print("直接访问：", swan.neck_swan)
print("直接访问：", swan.__neck_swan)
