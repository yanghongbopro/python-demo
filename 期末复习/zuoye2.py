# 【定义变量，并且把变量输出到控制台终端】 %s %d %f
# 定义三个变量，变量1名为 xingming,它的值是字符串：做题学生自己的姓名；变量2名为banji，它的值是字符串：网络班；变量3名为pythonfenshu,它的值是浮点数：85.5。
# 通过printf打印出：学生姓名：xingming，班级：网络班，python分数：81.5
xingming = "张权"
banji = "网络班"
pythonfenshu = 85.5
print("学生姓名：%s,班级：%s,python分数:%f" % (xingming,banji,pythonfenshu))


#【定义函数，并且判断该变量不同值输出不同内容】
# 定义函数名：huodong，参数为hour。函数内使用if elif else判断语句判断hour的值
# 如果hour == 20 ，通过print打印出：打8折活动开始；
# 否则，通过print打印出：活动尚未开始；

# 调用huodong函数，传递参数hour的值为21

def huodong(hour):
    if hour == 20 and hour == 21:
        print("打8折活动开始;")
    else:
        print("活动尚未开始；")
        


huodong(hour = 21)
    

#【定义列表并赋值，添加元素，遍历元素并打印】
# 定义列表变量名为 kecheng,赋值三个元素，分别是 数学 英语 体育
# 调用列表的添加元素函数，添加一个元素叫：美术
# 使用遍历方法，遍历列表变量kecheng，通过print打印出每个元素的值:数学 英语 体育 美术

kecheng = ["数学","英语","体育"]
kecheng.append("美术")
for a in kecheng:
    print(a)



#【定义一个类】
# 类名称：RenLei
# 构造函数里支持一个姓名参数，名称为xingming；
# 里面包含一个函数名为：chengji，包含除了第一个特殊self参数外，三个参数为：shuxue,yuwen,tiyu,该三个参数意思为传入数学分数，语文分数，体育分数的值
# chengji函数里分别打印出以下三种内容：
# 打印内容1（说明xxx的值取自参数shuxue,yuwen,tiyu对应的值）：学生姓名：（取构造函数里设置的xingming属性值），数学分数xxx，语文分数xxx，体育分数，xxx
# 打印内容2（说明总成绩取shuxue,yuwen,tiyu的值加起来）：学生姓名：（取构造函数里设置的xingming属性值），总成绩：xxx

# 实例化一个类的对象，对象变量名称叫xuesheng，构造时传xingming = "学生自己的姓名"；
# 通过实例对象xuesheng，调用chengji函数，传参的三门课程分数自己填浮点数，值随便填，是浮点数类型即可
class RenLei():
    def __init__(self,xingming) -> None:
        self.xingming = xingming

    def chengji(self,shuxue,yuwen,tiyu):
        print("学生姓名:%s，数学分数%f，语文分数%f，体育分数%f" % (self.xingming,shuxue,yuwen,tiyu))


xuesheng = RenLei(xingming = "张权") 
xuesheng.chengji(80.2,66.66,67.89)


