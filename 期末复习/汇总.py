#  复习范围知识点：定义变量，格式化打印输出变量内容，类型转换，运算符，分支选择语句，函数，列表，字典，元组，面向对象，类


# 【知识点：格式化打印输出--------------------------------------------------------------------------------------------------------】
# %s用来格式化字符串类型变量，%d用来格式化整型数字类型变量，%f用来格式浮点数类型变量
# 通过print打印输出多个变量值到控制台终端
name = "张三"
age = 17
shengao =1.78
print("学生名字：%s,年龄：%d,身高：%f。" % (name,age,shengao))

# 【知识点：类型转换】int函数转换为整数数字类型，float函数转换为浮点数小数类型，str函数转换为字符串类型
age_str = "17"
age_int = int(age_str)

jiage_str = "9.99"
jiage_float = float(jiage_str)

shengao_int = 188
shengao_str = str(shengao_int)


# 【知识点：关系运算符和逻辑运算符及选择分支及函数---------------------------------------------------------------------------------】
def info(a):
    if a == 0 or a == 1:
        print("a的值为0或者1")
    else:
        print("a不是0，也不是1")

# 调用上面定义的函数
info(1)

# 【知识点：函数定义及调用传递参数-------------------------------------------------------------------------------------------】
# 示例求两个参数的最小值
def min(x,y):
    if x < y:
        return x
    else:
        return y

#调用定义好的min函数
min_1 = min(16,69)
print("调用min函数结果是：%d" %(min_1))


# 【知识点：列表的定义，添加元素，查看元素，遍历元素-----------------------------------------------------------------------------】
#列表的定义
xs = ["zhangsan", "lisi", "wangwu"]

#添加元素
xs.append("赵六")

#查看元素索引下标为1的元素，并打印输出
xs_1 = xs[1]
print(xs_1)

#遍历元素,并通过print把每一个元素打印输出
for a in xs:
    print(a)



#【知识点：元组的定义，查看元素，遍历元素-------------------------------------------------------------------------------------------】
coffeename = ('蓝山', '卡布奇诺', '曼特宁', '摩卡', '麝香猫', '哥伦比亚')
#查看元素索引下标为1的元素，并打印输出
cf_1 = coffeename[1]
print(cf_1)

# 遍历元组并打印里面的元素
for item in coffeename:
    print(item)



#【知识点：字典的定义，添加元素，根据键key查找对应的值value，遍历元素-------------------------------------------------------------------】
sheng_gao = {"zhangsan":175,"lisi":166,"wangwu":180}

sheng_gao["zhaoliu"] = 178

lisi_sg = sheng_gao.get("lisi")
print("李四身高：%d" % (lisi_sg))

for k,v in sheng_gao.items():
    print("姓名:%s，身高：%d" %(k,v))


# 【定义类及实例化对象后调用里面的函数】
class XueSheng():
    def __init__(self):
        self.cj = {}
    
    def gou_wu(self,shou_ji,dian_nao):
        print("手机：%d,电脑：%d" % (shou_ji,dian_nao))
        zong_jia_ge = shou_ji + dian_nao
        print("购物总价格：%d" % zong_jia_ge)
    
    def chi_fan(self,shi_wu):
        print("吃了：%s" % shi_wu)

xuesheng_1 = XueSheng()
xuesheng_1.gou_wu(shou_ji=5999,dian_nao=3999)
xuesheng_1.chi_fan("只因")
