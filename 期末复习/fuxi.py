# 术语解释
# 代码缩进：一个tab键，或者4个空格
# 代码顶格：就是靠最左边，前面没有任何空格，代表这代码是独立新的，不属于任务冒号分支；

# print("hello world")

xingming = "张三"
age = 18
py_fenshu = 80.5

print("hello world")
print("hello world,姓名：%s 年龄：%d %.2f" %(xingming,age,py_fenshu))
print("hello world 分数：" + str(py_fenshu) + "年龄:" + str(age))

#
# # 变量名称week_no ，它代表的值是"星期六"
week_no = 6
if week_no == 6 or week_no == 7:
    # 代码缩进，冒号后面的代码缩进，4个空格，或者一个tab
    print("周末到了！")
    if week_no == 6:
        print("周六")
    else:
        print("周日")
else:
    # 代码缩进，冒号后面的代码缩进，4个空格，或者一个tab
    print("工作日。。。")
    


#代码缩进的场景有在遇到冒号后要缩进4个空格，选择语句if，for,while,函数，类

if 3 > 1:
    print("3 > 1")
else:
    print("3 < 1")
    


list1 = ["zhangsan","lisi","wangwu"]
for item in list1:
    print(item)

a = 12
b = 15
c = 0
if a>b:
    c = b
else:
    c = a


def min(a,b):
    c
    if a>b:
        c = b
    else:
        c = a
    return c

min(12,33)
min(122,33)

class Ren_Lei():
    def chi_fan(self,shi_wu):
        print("吃了%s" % (shi_wu))

    def min(self,a,b):
        c
        if a>b:
            c = b
        else:
            c = a
        return c

    def max(self,a,b):
        c
        if a>b:
            c = a
        else:
            c = b
        return c


#
# # 顶格的代码
# student_name = '张三'
# print("学生姓名：%s" % (student_name))
#
# # 学函数前：
# a = 12
# b = 3
# c = 0
# if a > b:
#     c = a
# else:
#     c = b
# print("最大值是：%d" % (c))
#
# d = 13
# f = 21
# g = 0
# if d > f:
#     g = d
# else:
#     g = f
# print("最大值是：%d" % (g))
#
# #定义函数max，记住定义函数没被其他地方调用，里面的代码是不会被执行的，直到有地方调用，比如max(11,9) 得到结果：11
# def max_2a3b(a, b):
#     if a > b:
#         return a
#     else:
#         return b
#
# #函数调用
# zuidazhi = max_2a3b(a = 10,b = 22)
# print("最大值：" + str(zuidazhi))
#
#
# # 循环语句
# list1 = ["张三","李四"]
# for item in list1:
#     print(item)
#
# #定义一个类
# class laoban():
#
#     def __init__(self):
#         self.gongzi = {}
#
#
#
#     def max(self,a,b):
#         if a > b:
#             return a
#         else:
#             return b
#
#
#
#
#
# chengjis = {'zhangsan': 12, 'lisi': 22, 'wangwu': 33}
# zong_cheng_ji = 0
# for k,v in chengjis.items():
#     zong_cheng_ji = zong_cheng_ji + v
#
# print("算出的总成绩%d" %(zong_cheng_ji))