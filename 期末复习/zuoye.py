# 定义一个老板类：
# 函数1：录入工人工资：使用字典形式
# 函数2：打印所有工人的工资
# 函数3：计算工人平均工资
# 函数4：计算工人总工资多少：字典的for循环
# 函数5：获取某个工人的工资


# 实例化一个老板对象
# 调用函数 录入工人工资
# 调用函数 计算工人总的工资
# 调用函数 计算工人工资平均工资
# 调用函数 打印所有工人工资
# 调用函数 打印某个工人的工资出来


class laoban():

    def __init__(self):
        self.gongzi = {}

    # 录入工资 {"zhangsan":55,"lisi":66,"wangwu":77}
    def luru(self,**gongzi):
        self.gongzi = gongzi
    
    # 打印所有工资
    def dayin(self):
        for k,v in self.gongzi.items():
            print("名称：%s，成绩：%d" % (k,v))
            
    # 总工资
    def zong(self):
        money = 0
        # 遍历字典
        for k,v in self.gongzi.items():
            money = money + v
        return money
    

    def pingjun(self):
        money = 0
        for k,v in self.gongzi.items():
            money = money + v

        renshu = len(self.gongzi)
        pj = money/renshu
        pj2 = int(pj)
        return pj2


    def chazhao(self,xingming):
        gj = self.gongzi.get(xingming)
        return gj



wangfugui = laoban()

wangfugui.luru(张三 = 2000,lisi = 3000,wangwu = 2333)
# 录入工资 {"zhangsan":2000,"lisi":3000,"wangwu":2333}
# self.gongzi =  {"zhangsan":2000,"lisi":3000,"wangwu":2333}
# wangfugui.gongzi =  {"zhangsan":2000,"lisi":3000,"wangwu":2333}

wangfugui.dayin()

money1 = wangfugui.zong()
print("总工资:" + str(money1))

pjgj = wangfugui.pingjun()
print("平均工资:" + str(pjgj))

lisigj = wangfugui.chazhao("lisi")
print("lisi工资为%d" % (lisigj))