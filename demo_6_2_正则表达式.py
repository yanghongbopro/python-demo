import re

pattern = r'(13[4-9]\d{8})$|(15[01289]\d{8})$'

mobile = '13634222222'
match = re.match(pattern, mobile)  # 进行模式匹配
if match == None:  # 判断是否为None,为真表示匹配失败
    print(mobile, '不是有效的中国移动手机号码。')
else:
    print(mobile, '是有效的中国移动手机号码。')

mobile = '13144222221'
match = re.match(pattern, mobile)  # 进行模式匹配
if match == None:
    print(mobile, '不是有效的中国移动手机号码。')
else:
    print(mobile, '是有效的中国移动手机号码。')



pattern = r'(黑客)|(抓包)｜(监听)|(Trojan)'  # 模式字符串
about = '我是一名程序员，我喜欢看黑客方面的图书，想研究一下Trojan。'
match = re.search(pattern, about)  # 进行模式匹配
if match == None:  # 判断是否为None,为真表示匹配失败
    print(about, '安全!')
else:
    print(about, '出现了危险词汇!')

about = '我是一名程序员，我喜欢看计算机网络方面的图书，喜欢开发网站。'
match = re.match(pattern, about)
if match == None:
    print(about, '安全!')
else:
    print(about, '出现了危险词汇!')



pattern = r'1[34578]\d{9}'  # 定义要替换的模式字符串
string = '中奖号码为：84978981 联系电话为：13611111111'
result = re.sub(pattern, '1XXXXXXXXXX', string)  # 替换字符串
print(result)
