
class Fruit:  # 定义水果类(基类)
    color = "绿色"  # 定义类属性

    def harvest(self, color):
        print("水果是：", color, "的！")  # 输出的是形式参数color
        print("水果已经收获......")
        print("水果原浆是：", Fruit.color + "的！")  # 输出的是类属性color


class Apple(Fruit):  # 定义苹果类(派生类)
    color = "红色"

    def __init__(self):
        print("我是苹果")


class Orange(Fruit):  # 定义橘子类(派生类)
    color = "橙色"

    def __init__(self):
        print("\n我是橘子")

    def harvest(self, color):
        print("橘子是", color, "的！")
        print("橘子已经收获......")
        print("橘子原来是：" + Fruit.color + "的！")


apple = Apple()
apple.harvest(apple.color)
orange = Orange()
orange.harvest(orange.color)
