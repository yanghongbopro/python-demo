# 作业：
# 定义一个类-学生类，类名叫Student，作为基类（也叫父类，可供其他有继承需求的子类来继承它，以达到复用该类的属性和函数方法）里面包含
# 1、一个类属性：眼睛eyes，并在声明时赋值为2；
# 2、一个实例属性：姓名name，并在初始化构造函数里接收该参数赋值给实例属性。
# 3、一个行为方法：吃饭eat，吃饭方法函数里面打印出第2点的实例属性name有eyes个眼睛，在吃饭。比如打印出=》 张三有2个眼睛，在吃饭。

# 定义一个派生类，类名叫CityStudent，继承于学生类Student,里面包含
# 1、一个类属性：学校名称 school_name，并在声明时赋值为 惠州城市职业学院
# 2、一个实例属性：班级名称 class_name；并在初始化构造函数里接收该参数赋值给实例属性。
# 3、一个实例属性：课程成绩字典 score_dict;初始化时赋值为空字典；
# 4、一个行为方法：录入学生的成绩 add_scores，方法接收两个参数，一个是课程名称，一个是分数，其中课程名称作为键key，分数作为值value
# 5、一个行为方法：打印学生成绩print_scores，打印出学生的姓名+来自学校名称+班级名称+遍历学生的课程成绩字典 score_dict，把每一门课程的名称和分数都打印出来；（可以加@property改为属性调用，选做，不是必做该动作）

# 使用上面类的定义的内容实现以下效果：
# 1、实例化一个CityStudent对象，在实例化的小括号里传参数name="张三"，class_name="网络班"

# 2、调用吃饭函数：结果，打印出=》张三有2个眼睛，在吃饭。
# 3、调用录入成绩函数保存在实例属性score_dict（字典类型），录入：python 80，riyu 70，yingyu 60, =》录入后数据效果相当于score_dict = {'python': 80, 'tiyu': 70, 'yingyu': 60}
# 4、调用打印成绩函数：输出示例：
# 我叫张三来自城市职业学院网络班,我的成绩为：
# 课程：python,成绩：80
# 课程：tiyu,成绩：70
# 课程：yingyu,成绩：60