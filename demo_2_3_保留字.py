# 导入系统自带的保留字（也叫关键词）模块
import keyword

# 打印输出保留字,查看哪些名字是系统保留字，记住保留字不能用来给自己的变量或者函数等起名字
print(keyword.kwlist)

# 示例1：判断 and 是否是关键词
is_keyword = keyword.iskeyword("and")
# and = '我是保留字'，如果是保留字，那么and不能用命名变量或者函数等
if is_keyword == True:
    print("and 是保留字")
    # and = "and 是保留字,定义一个变量的名称是and会报错，不信你反注释该句语句"
else:
    print("and 不是保留字")


# 示例2：判断 name 是否是关键词，如果不 是保留字，那么name不能用命名变量或者函数等
is_keyword_2 = keyword.iskeyword("name")

if is_keyword_2 == True:
    print("name 是保留字")
else:
    print("name 不是保留字")
    name = "张三"
    print("name可以用来起名字给变量" + name)