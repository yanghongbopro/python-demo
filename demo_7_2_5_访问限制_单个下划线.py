from json_util import JsonUtil

class Swan:
    """天鹅类"""
    _neck_swan = '天鹅第脖子很长'
    no_xiahuaxiang_neck_swan = '没有下划线的'
    def __init__(self,df = "123"):
        # self.no_xiahuaxiang_neck_swan = df
        print("__init__():", Swan._neck_swan)
        print("__init__():", Swan.no_xiahuaxiang_neck_swan)
        # print("__init__():", self.no_xiahuaxiang_neck_swan)


swan1 = Swan()
print("直接访问：", swan1._neck_swan)
print("直接访问：", swan1.no_xiahuaxiang_neck_swan)
print("直接访问：", Swan.no_xiahuaxiang_neck_swan)
Swan.no_xiahuaxiang_neck_swan = "123456"

# swan2 = Swan(df="456")
# print("直接访问：", swan2._neck_swan)
# print("直接访问：", swan2.no_xiahuaxiang_neck_swan)
# print("直接访问：", Swan.no_xiahuaxiang_neck_swan)
# jsonUt = JsonUtil()
# print(jsonUt.ToJson(swan1))
# print(jsonUt.ToJson(swan2))
