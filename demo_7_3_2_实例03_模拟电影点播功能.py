from json_util import JsonUtil
class TVshow:
    list_film = ['夺冠', '我和我的家乡', '姜子牙']

    def __init__(self, show):
        self.__show = show

    @property
    def show_a(self):
        return self.__show

    @show_a.setter
    def show_a(self, value):
        if value in TVshow.list_film:
            self.__show = "您选择了《" + value + "》，稍后将播放"
        else:
            self.__show = "您点播的电影不存在"

json = JsonUtil()
tvshow = TVshow("夺冠")
print("正在播放：《", tvshow.show_a, "》")
print("您可以从", tvshow.list_film, "中选择要点播的电影")
tvshow.show_a = "姜子牙"
print(json.ToJson(tvshow))
print(tvshow.show_a)
# print(tvshow.__show)

