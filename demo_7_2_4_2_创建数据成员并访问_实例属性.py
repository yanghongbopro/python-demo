from json_util import JsonUtil
class Geese:
    """大雁类"""
    neck = "脖子较长"
    wing = "展翅频率高"
    leg = "腿位于身体的中心支点，行走自如"
    number = 0  # 编号

    def __init__(self,leg):  # 构造方法
        self.leg = leg
        Geese.number += 1
        # print("\n我是第" + str(Geese.number) + "只大雁!我有下面的特征")
        # print(Geese.neck)
        # print(Geese.leg)
        # print(Geese.wing)


json = JsonUtil()

aaa = Geese(leg="heheh")
print(aaa.leg)
print(Geese.leg)
Geese.leg = "hoiuiui"
print(aaa.leg)
print(Geese.leg)
print(aaa.neck)
aaa.neck = "dddddddddddddddd"
print(aaa.neck)
print(Geese.neck)
del aaa.neck
print(aaa.neck)
aaa.dddd = 1232
print(aaa.dddd)
print(json.ToJson(aaa))
