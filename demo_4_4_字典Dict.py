# 定义字典 {"name":"张三"}
chengjis = {'张三': 12, '李四': 22, '王五': 33}

zongfen = 0
for k,v in chengjis.items():
    zongfen = zongfen + v
    print("key:%s,value:%s" % (k,v))

print("zongfen:" + str(zongfen))
# print("zongfen:%s" % (str(zongfen)))
print(chengjis.get("Lily")) # 22

# dictionary_2 = dict()
# tuple_1 = (2,"3")
# dictionary_3 = {1: '12345678', tuple_1: '12345679', 'Helen': {"name":"lily"}}
# print(dictionary)
# print(dictionary_2)
# print(dictionary_3)

# print(dictionary.get("Helen")) #不存在的key不会报错

# # 读取元素
# print(dictionary['Lily'])
# # print(dictionary["tom"]) #不存在的key会报错
# print(dictionary.get("Helen")) #不存在的key不会报错
# print(dictionary.get("tom","110"))
# # print(dictionary_3.get((2,"3")))
# # print(dictionary_3.get(tuple_1))

# # 遍历字典数据
# print(dictionary.items())
# print(dictionary.keys())
# print(dictionary.values())

# for k,v in dictionary.items():
#     print("key:%s,value:%s" % (k,v))

# # 添加元素
# dictionary["tom"] = "222"
# print(dictionary.get("tom"))

# # 字典推导
# import random  # 导入random标准库
# randomdict = {i: random.randint(10, 100) for i in range(1, 5)}
# print("生成的字典为:", randomdict)