#!/usr/bin/env python
# encoding: utf-8
'''
@author: burette
@file: geese_a.py
@desc:
'''


class Geese:
    """大雁类"""
    neck = "脖子较长"
    wing = "展翅频率高"
    leg = "腿位于身体的中心支点，行走自如"
    number = 0  # 编号

    def __init__(self):  # 构造方法
        Geese.number += 1
        print("\n我是第" + str(Geese.number) + "只大雁!我有下面的特征")
        print(Geese.neck)
        print(Geese.leg)
        print(Geese.wing)



list1 = []
for i in range(4):
    list1.append(Geese())
print("总共有" + str(Geese.number) + "只大雁")

Geese.beak = "喙的基部较高,长度和头部的长度几乎相等" # 添加类属性
print("第3只大雁的喙:", list1[2].beak)
