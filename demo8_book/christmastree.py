#!/usr/bin/env python
# encoding: utf-8
'''
@author: burette
@file: christmastree.py
@desc:
'''

pinetree = '我是一棵松树'  # 定义一个全局变量(松树)


def fun_christmastree():  # 定义函数
    '''功能:一个梦
    无返回值
    '''
    pinetree = '挂上彩灯、礼物......我变成一棵圣诞树\n'  # 定义局部变量
    print(pinetree)  # 输出局部变量的值

print("__name__: " + __name__)
# print('\n下雪了......\n')
# print('...开始做梦...\n')
# fun_christmastree()  # 调用函数
# print('...梦醒了...\n')
# pinetree = '我身上落满雪花,' + pinetree + '\n'  # 为全局变量赋值
# print(pinetree)  # 输出全局变量的值

if __name__ == "__main__":
    print('\n下雪了......\n')
    print('...开始做梦...\n')
    fun_christmastree()  # 调用函数
    print('...梦醒了...\n')
    pinetree = '我身上落满雪花,' + pinetree + '\n'  # 为全局变量赋值
    print(pinetree)  # 输出全局变量的值
