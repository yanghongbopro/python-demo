def fun_bmi(person, height, weight):
    """
    功能：根据身高和体重计算BMI指数
    :param person:姓名
    :param height:身高
    :param weight:体重
    :return:
    """
    print(person + "的身高：" + str(height) + "\t体重：" + str(weight))
    bmi = weight / (height * height)
    print(person + "的BMI指数为：" + str(bmi))
    # 省略显示的相关代码

def fun_bmi_upgrade(*person):
    """
    功能：根据身高和体重计算BMI指数（升级版）
    :param person:
    :return:
    """
    # 省略函数主题代码