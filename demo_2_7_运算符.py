
# # 7.1 算术运算符 => 开始
# python_scores = 95
# english_scores = 92
# c_scores = 89

# sub = python_scores - english_scores # sub = 3
# total_scores = python_scores + english_scores + c_scores
# avg = total_scores / 3

# print('python分数减去english分数为：%d' % sub)
# print('总分数为：%d' % total_scores)
# print('平均分为：%d' % avg)

# # 7.1 算术运算符 => 结束


# # 7.2 赋值运算符 => 开始
# a = 11
# b = 55
# c = a + b
# print('c = %d' % c)

# # 相当于 a = a + b
# a += b
# print('a = %d' % a)

# # 相当于 a = a * b
# a *= b
# print('a = %d' % a)

# # 相当于 a = a / b
# a /= 332
# print('a = %f' % a)

# c += 1
# # 相当于 c = c % b 求余数
# c %= 5
# print('c = %d' % c)

# # 7.2赋值运算符 => 结束


# # 7.3 比较运算符 => 开始

# print("python分数：%d，english分数：%d，C语言分数：%d。\n" % (python_scores, english_scores, c_scores))
# bj_1 = python_scores < english_scores
# bj_2 = python_scores > english_scores
# bj_3 = python_scores == english_scores
# bj_4 = python_scores != english_scores
# bj_5 = python_scores <= english_scores
# bj_6 = python_scores >= c_scores

# print('python_scores < english_scores 的结果是：' + str(bj_1))
# print('python_scores > english_scores 的结果是：' + str(bj_2))
# print('python_scores == english_scores 的结果是：' + str(bj_3))
# print('python_scores != english_scores 的结果是：' + str(bj_4))
# print('python_scores <= english_scores 的结果是：' + str(bj_5))
# print('python_scores >= c_scores 的结果是：' + str(bj_6))

# # 7.3 比较运算符 => 结束


# # 7.4 逻辑运算符 => 开始  and or
# lj_1 = 3 > 2 # True
# lj_2 = 2 > 5 # False
# lj_3 = 10 >= 9 # True
# lj_4 = 10 == 9 # False

# # and ： 同时是真，才是真True，其他都是假False
# # or： 同时是假，才是假，其他都是真 if True or False:

# if lj_1 and lj_2: # =>if True and False: =>False

#     print("lj_1 = %s,lj_2 = %s, lj_1 and lj_2 = %s \n" % (str(lj_1), str(lj_2), 'True'))
# else:
#     print("lj_1 = %s,lj_2 = %s, lj_1 and lj_2 = %s \n" % (str(lj_1), str(lj_2), 'False'))
    
# if lj_1 and lj_3:
#     print("lj_1 = %s,lj_3 = %s, lj_1 and lj_3 = %s \n" % (str(lj_1), str(lj_3), 'True'))
# else:
#     print("lj_1 = %s,lj_3 = %s, lj_1 and lj_3 = %s \n" % (str(lj_1), str(lj_3), 'False'))

# if lj_1 or lj_2:
#     print("lj_1 = %s,lj_2 = %s, lj_1 or lj_2 = %s \n" % (str(lj_1), str(lj_2), 'True'))
# else:
#     print("lj_1 = %s,lj_2 = %s, lj_1 or lj_2 = %s \n" % (str(lj_1), str(lj_2), 'False'))

# if lj_4 or lj_2:
#     print("lj_4 = %s,lj_2 = %s, lj_4 or lj_2 = %s \n" % (str(lj_4), str(lj_2), 'True'))
# else:
#     print("lj_4 = %s,lj_2 = %s, lj_4 or lj_2 = %s \n" % (str(lj_4), str(lj_2), 'False'))
    
# if not lj_1:
#     print("lj_1 = %s, not lj_1 = %s \n" % (str(lj_1), 'True'))
# else:
#     print("lj_1 = %s, not lj_1 = %s \n" % (str(lj_1), 'False'))

# # 7.4 逻辑运算符 => 结束


# 7.5 示例 => 开始
print("手机店正在打折，活动进行中。。。")
str_week = input("请输入中文星期（如星期一）：")
int_time = int(input("请输入时间中的小时（范围：0 ~ 23）："))

if (str_week == "星期一" and (int_time >= 10 and int_time <= 11)) or (str_week == "星期四" and (int_time >= 14 and int_time <= 15)):
    print("恭喜你，获得折扣活动参与资格，快快选购吧")
else:
    print("不在活动时间范围内！")
    
# 7.5 示例 => 结束
