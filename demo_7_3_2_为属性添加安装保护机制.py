class TVshow:
    def __init__(self, show):
        self._show = show

    @property  # 将方法转换为属性
    def show(self):  # 定义show()方法
        return self._show  # 返回私有属性的值
    
    @show.setter  # 将方法转换为属性
    def show(self, value):  # 定义show()方法
        self._show = value  # 设置私有属性的值

tvshow = TVshow("正在播放《我是歌手》")  # 创建类的实例
print("默认：", tvshow._show)  # 获取属性值
print("默认：", tvshow.show)  # 获取属性值
tvshow._show = "abc"
print("默认：", tvshow._show)  # 获取属性值
tvshow.show = "正在播放《快乐大本营》"  # 修改属性值
print("修改后 : ",tvshow.show)  # 获取属性值