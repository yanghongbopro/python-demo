# # 数字类型：整数
# int1 = 21231
# print('整数变量名int1的值是：%d' % (int1))

# # 数字类型：浮点数
# pi = 3.1415
# print('整数变量名float_number的值是：%f' % pi)

# # 字符串类型
# str_name = "网络班"
# str_duohang = '''
# 这是第一行字符串
# 这是第二行字符串
# 这是第三行字符串
# '''
# str_duohang = """
# 这是第一行字符串
# 这是第二行字符串
# 这是第三行字符串- 双引号
# """
# print('字符串变量名str_name的值是：%s' % str_name)
# print('字符串变量名str_duohang的值是：%s' % str_duohang)

# 布尔类型，真True 和 假 False
# boolean_true = True
boolean_true = 3 > 2
boolean_false = False
# print('布尔变量名boolean_true的值是：%s' % str(boolean_true))
# print('布尔变量名boolean_false的值是：%s' % str(boolean_false))

if True:
    print('布尔变量名boolean_true的值是：%s' % str(boolean_true))
else:
    print("False")

if boolean_true:
    print('布尔变量名boolean_true的值是：%s' % str(boolean_true))
else:
    print("False")