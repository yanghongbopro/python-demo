
# 【字符串拼接】
mot_en ='Happy Birthday.'
mot_cn = '生日快乐。'
print(mot_en + ' – ' + mot_cn)

 

# 对字符串和整数进行拼接，需先将整数类型转换为字符串
str1 = '我今天一共走了'  # 定义字符串
num = 12098  # 定义一个整数
str2 = "步"  # 定义字符串
print(str1 + str(num) + str2)
 
# 换行符 \n
programmer_1 = '程序员甲：搞IT太辛苦了,我想换行......怎么办?'
programmer_2 = '程序员乙：敲一下回车键'
print(programmer_1 + '\n' + programmer_2)



str1='人生苦短，我用Python!'  #定义字符串

# 【计算字符串长度】 len函数
str1_len = len(str1)
print(str1 + "=>长度len的值为：" + str(str1_len))

str1_encode_len = len(str1.encode())
print(str1 + "=>encode后长度len的值为：" + str(str1_encode_len))




# 【字符串截取】
str1='人生苦短，我用Python!'  #定义字符串
substr1 = str1[1:2]   #截取第2个字符
substr2 = str1[5:]   #从第6个字符截取
substr3 = str1[:5]   #从左边开始截取5个字符
substr4 = str1[2:5]   #截取第3个到第5个字符
print('原字符串：', str1)
print(substr1+'\n'+substr2+'\n'+substr3+'\n'+substr4)


# 【字符串分割】 split
str1 = "张三 李四 王五&&lily tony juli&&luomiou daqiang cuihua"
print('原字符串:',str1)
list1 =str1.split()  # 采用默认分隔符进行分割：=》['张三', '李四', '王五&&lily', 'tony', 'juli&&xiaohong', 'daqiang', 'cuihua']
list2 = str1.split('&&')   # 采用多个字符进行分割
list4 = str1.split(' ',4)  # 采用空格进行分割,并且只分割前4个
print(str(list1) + '\n' + str(list2) + '\n' + str(list4))


# 【字符串合并】 join
list_friend = ["百度科技", "李彦宏", "马云", "马化腾"]  # 好友列表
str_friend = '@'.join(list_friend)  # 用空格+@符号进行连接
at = '@' + str_friend  # '#由于使用join()方法时,第一个元素前不加分隔符,所以需要在前面加上@符号
print('您要@的好友:', at)


# 【检索字符串】- 指定字符串出现的次数 count
str1 = '@百度科技@李彦宏@命俞敏洪'
print('字符串', str1, '中包括', str1.count('@'), '个@符号')



# 【检索字符串】- 指定字符串首次出现的位置 find
# 不存在返回-1，不会异常
str1 = '@百度科技@李彦宏@俞敏洪'
print('字符串', str1, '中@符号首次出现的位置索引为:', str1.find('@'))



# 【检索字符串】- 指定字符串首次出现的位置 index
# 不存在抛异常
str1 = '@百度科技@李彦宏@俞敏洪'
print('字符串', str1, '中@符号首次出现的位置索引为：', str1.index('@'))
# print('字符串', str1, '中@符号首次出现的位置索引为：', str1.index('#'))




# 【去除特殊符号】去除字符串中的空格和特殊字符
str1 = "   abc       "
str1_strip = str1.strip()
str1_lstrip = str1.lstrip()
str1_rstrip = str1.rstrip()
print("原字符串：" + str1)
print("原字符串strip()后：" + str1_strip)
print("原字符串lstrip()后：" + str1_lstrip)
print("原字符串rstrip()后：" + str1_rstrip)

str1 = "@@abc@@@@@"
str1_strip = str1.strip("@")
str1_lstrip = str1.lstrip("@")
str1_rstrip = str1.rstrip("@")
print("原字符串：" + str1)
print("原字符串strip()后：" + str1_strip)
print("原字符串lstrip()后：" + str1_lstrip)
print("原字符串rstrip()后：" + str1_rstrip)


# 字符串编码解码
verse = "野渡无人舟自横"
byte = verse.encode('GBK')
print("原字符串：", verse)
print("转换后：", byte)

