# 定义类
class Student():
    # 构造函数：实例化类时自动调用该函数
    def __init__(self,xm):
        self.name = xm
    
    def eat(self,shi_wu):
        print("%s在吃。" % (self.name,shi_wu))


# 使用类，通过实例化构造该类的一个具体的对象，比如学生类下的张三这个具体的学生！
# 参数：xm = "张三"，是隐式调用了__init__这个函数，所以这里传参是传给__init__的xm参数的
# zhang_san:是实例化Student类后的一个对象，这时候可以使用zhang_san.函数(参数)格式调用类里定义的函数了
zhang_san = Student(xm = "张三")
# 参数：shi_wu = "鸡翅膀"，是调用了Student定义的函数def eat(self,shi_wu)
zhang_san.eat(shi_wu = "鸡翅膀")


class CityStudent(Student):

    school_name = "城市职业学院"

    def __init__(self, xm,class_name):
        super().__init__(xm)
        self.class_name = class_name
        self.score_dict = dict()
    
    def add_scores(self,**scores):
        self.score_dict = scores

    def print_scores(self):
        print("我叫%s来自%s%s,我的成绩为：" % (self.name, CityStudent.school_name,self.class_name))
        print(self.score_dict)
        for k,v in self.score_dict.items():
            print("课程：%s,成绩：%d"% (k,v))




zhangsan = CityStudent("张三","网络班")
zhangsan.eat(shi_wu="炒饭")
zhangsan.add_scores(python=80,tiyu=70,yingyu=60)
zhangsan.print_scores()