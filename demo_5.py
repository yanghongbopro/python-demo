
# 1、定义一个函数
def hello() :
    print("Hello World!")

# 2、调用已定义的函数
hello()

# 3、定义带参数的函数
def max(a, b):
    if a > b:
        return a
    else:
        return b

# 4、调用带参数的函数
a = 4
b = 5
result_4 = max(a, b)
print(result_4)

# 5、必须参数
def printme(str):
   print(str)
   return
 
# 调用 printme 函数，不加参数会报错
# printme()

# 7、关键字参数
def printinfo_7(name, age):
   print ("名字: ", name)
   print ("年龄: ", age)
   return
 
# 使用关键字调用
printinfo_7(age=18, name="张三" )
# 不使用关键字调用：错误顺序的使用
printinfo_7(19, "李四" )
# 使用关键字调用：正确顺序的使用
printinfo_7("王五", 17 )

# 8、默认参数
def printinfo_8(name, age = 18):
   print ("名字: ", name)
   print ("年龄: ", age)
   return
 
#调用printinfo_8函数
printinfo_8(age=19, name="tom" )
print("------------------------")
printinfo_8("lily")


# 9、不定长参数,一个星号*，参数以元组类型传递
def printinfo_9(arg1, *vartuple ):
   print("输出: ")
   print(arg1)
   print(type(vartuple))
   print(vartuple)
   for p in vartuple:
       print(p)
 
# 调用printinfo_9 函数
printinfo_9(70, 60, 50)
printinfo_9(70, 60, 50, 20, 11)



# 10、不定长参数,两个星号 **， 参数以字典的形式传递
def printinfo_10(arg1, **vardict):
   print ("输出: ")
   print (arg1)
   print(type(vardict))
   print (vardict)
   for k,v in vardict.items():
       print("键是：%s,值是：%s" % (k,str(v)))
 
# 调用printinfo_10函数
printinfo_10(1, name = "zhangsan",age = 18, python_score = 90.5)

printinfo_10(1, name = "lisi",age = 19, python_score = 70.5)



# 11、匿名函数 lambda
sum = lambda arg1, arg2: arg1 + arg2
 
# 调用sum函数
print ("相加后的值为 : ", sum( 10, 20 ))
print ("相加后的值为 : ", sum( 20, 60 ))


# 12、return语句
def sum(arg1, arg2 ):
   # 返回2个参数的和."
   result = arg1 + arg2
   print ("函数内 : ", result)
   # 用于返回给调用方使用
   return result
 
# 调用sum函数，使用一个变量比如total，用来接收sum函数执行完成后return的值，
total = sum(10, 20 )
print ("函数外 : ", total)