# 纯数字列表
set1 = {1, 4, 2, 7, 13, 43, 25, 8, 10}
# 纯字符串
set2 = {"渔舟唱晚", "高山流水", "出水莲", "汉宫秋月"}
set3 = {'Python', 28, ("人生苦短", "我用Python"), (22, "自动化运维", "云计算", "Web开发")}
print(set1)
print(set2)
print(set3)

# 列表转换为集合
list_1 = [1, 4, 2, 7, 13, 43, 25, 8, 10]
set4 = set(list_1)
print(set4)

# range对象转换为集合
range_1 = range(1,10,2)
set5 = set(range_1)
print(set5)

# 集合添加元素
set5.add(11)
print(set5)


# 并集
print(set4 | set5)

# 交集
print(set4 & set5)

# 差集
print(set4 - set5)
