class Fruit:
    def __init__(self, color="绿色"):
        Fruit.color = color

    def harvest(self, color):
        print("水果是：", color, "的！")  # 输出的是形式参数color
        print("水果已经收获......")
        print("水果原来是：", Fruit.color + "的！")  # 输出的是类属性color


class Apple(Fruit):  # 定义苹果类(派生类)
    color = "红色"

    def __init__(self):
        print("我是苹果")
        super().__init__()


class Sapodilla(Fruit):
    def __init__(self, color):
        print("\n我是人参果")
        super().__init__()

    def harvest(self, color):
        print("人参果是：" + color + "的！")
        print("人参果已经收获......")
        print("人参果原来是：" + Fruit.color + "的！")

sapodilla = Sapodilla("白色")
sapodilla.harvest("金黄色带紫色条纹")

apple = Apple()
apple.harvest(apple.color)

