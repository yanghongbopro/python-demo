

def hello() :
    print("Hello World!")


    hello()


def max(a, b):
    if a > b:
        return a
    else:
        return b


a = 3
b = 5
result_4 = max(a, 8)
print(result_4)



def printme(str):
   print(str)
   return
 

printme()

# 7、关键字参数
def printinfo_7(name, age):
   print ("名字: ", name)
   print ("年龄: ", age)
   return
 
# 使用关键字调用,其中name为张三，age为18，请问用关键字传参如何传？
printinfo_7()


# 8、默认参数
def printinfo_8(name, age = 18):
   print ("名字: ", name)
   print ("年龄: ", age)
   return

printinfo_8("lily")



def printinfo_9(arg1, *vartuple ):
   print("输出: ")
   print(arg1)
   print(type(vartuple))
   print(vartuple)
   for p in vartuple:
       print(p)
 
# 调用printinfo_9 函数
# 怎么传递参数



def printinfo_10(arg1, **vardict):
   print ("输出: ")
   print (arg1)
   print(type(vardict))
   print (vardict)
   for k,v in vardict.items():
       print("键是：%s,值是：%s" % (k,str(v)))
 
# 调用printinfo_10函数




# 12、return语句
def sum(arg1, arg2 ):
   # 返回2个参数的和."
   result = arg1 + arg2
   print ("函数内 : ", result)
   # 用于返回给调用方使用
   return result
 
# 调用sum函数，使用一个变量比如total，用来接收sum函数执行完成后return的值，
total = sum(10, 20 )
print ("函数外 : ", total)