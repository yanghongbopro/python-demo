
# 7.2.3 知识点 创建一个类，和init方法
class Geese:
    """大雁类"""
    def __init__(self):  # 构造方法
        print("我是大雁类!")

geese = Geese()  # 实例化一个对象
