# 定义了一个班级类
class Bangji():

    # 构造方法：用于在实例化类的具体对象时，初始化设置一些属性值，比如设置班级的实例属性class_name，students
    # 【知识点】__init__构造函数的作用以及使用。
    def __init__(self,bangji) -> None:
        self.class_name = bangji
        self.students = [] # 注意：班级的实例属性的students是一个列表，中括号括起来
    
    # 添加学生到班级的实例属性students列表里
    def add(self,student):
        # 把参数student的值添加到实例属性students列表里，【知识点】[demo_4_2_列表List.py] # 添加元素，调用列表自带的函数append
        self.students.append(student)
    
    # 通过姓名查找学生，
    # 【知识点】
    # 1、[demo_4_2_列表List.py] # 遍历   for item in self.students:
    # 2、[demo_2_7_运算符.py] #7.4 逻辑运算符 的 ==   表示左右是否相等
    # 3、[demo_3_2_1_2_if_else语句.py] if语句
    # 4、[demo_3_5_跳转语句break和continue.py] break语句的用法
    def find(self,xingming):
        jie_guo = None

        # 1、[demo_4_2_列表List.py] # 遍历   for item in self.students:    
        for item in self.students:
            # 3、[demo_3_2_1_2_if_else语句.py] if语句
            # 2、[demo_2_7_运算符.py] #7.4 逻辑运算符 的 ==   表示左右是否相等
            if item.name == xingming:
                jie_guo = item
                break # 4、[demo_3_5_跳转语句break和continue.py] break语句的用法
        
        return jie_guo

    
