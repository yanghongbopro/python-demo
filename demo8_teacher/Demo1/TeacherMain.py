# import 模块名   =>   通过import 后面加模块名字 导入一个普通模块（里面有变量，函数），as 后面的gj是别名，gj代表GongJu，主要是用于简写，或者避免重名
import GongJu as gj

# from 模块名 import 模块里的函数成员shi_fou_ji_ge   => from 意思是从某个包的某个模块里，然后import 写模块里的成员：变量，函数，类等等
from GongJu import shi_fou_ji_ge

# from 包名.模块名 import 模块里面的类成员StudentInfo  => from 意思是从某个包的某个模块里，然后import 写模块里的成员：变量，函数，类等等
from model.StudentInfoModel import StudentInfo

# from 包名.模块名 import 模块里面的类成员StudentInfo  => from 意思是从某个包的某个模块里，然后import 写模块里的成员：变量，函数，类等等
from dal.BangJiDal import Bangji




if __name__ == "__main__":
    print("老师开始操作教务系统。")

    # 【第一步】创建班级：通过班级类构建具体的班级对象
    wang_luo_ban = Bangji("网络班")

    # 【第二步】创建学生：通过学生类构建具体的学生对象,并且每个学生对象都录入一次成绩
    zhangsan = StudentInfo("张三",18)
    python_chengji = gj.kao_shi() # 只演示了python课程是调用考试函数算得分的。其他分数随便写。
    # 调用录分数函数，把学生的成绩通过不定长参数（字典方式传参）传给学生的成绩属性
    zhangsan.lu_cheng_ji(python = python_chengji,shu_xue=80,ti_yu = 59)


    lisi = StudentInfo("李四",19)
    python_chengji = gj.kao_shi()
    lisi.lu_cheng_ji(python = python_chengji,shu_xue=81,ti_yu = 91)

    #【第三步】把学生加入到班级：把创建好的学生加到班级对象的实例属性students列表里
    # 调用班级里的函数 add，把构造出来的学生实例对象添加到班级里的实例属性里：
    wang_luo_ban.add(zhangsan)
    wang_luo_ban.add(lisi)


    # 【第四步】打印全部学生成绩：遍历网络班对象里的属性students，并调用每个学生对象的info函数，打印学生的成绩出来
    students = wang_luo_ban.students
    for item in students:
        item.info()


    # 【第五步】在班级里查找某个学生，并判断是否及格：查张三这个学生出来，看看他python分数是否及格：
    zs = wang_luo_ban.find(xingming= "张三")
    zs_python_fen_shu = zs.kan_fen_shu("python")
    ji_ge = shi_fou_ji_ge("python",zs_python_fen_shu)
    if not ji_ge:
        print("%s python分数为:%d,不及格，DA一顿？" % (zs.name,zs_python_fen_shu))
    else:
        print("%s python分数为:%d,及格了，CUO一顿？" % (zs.name,zs_python_fen_shu))
