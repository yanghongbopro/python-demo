# 一、（分数10）：定义变量及把这些变量打印输出，按以下要求编写代码：
# 1.定义三个变量，变量1名为 school,它的值是：城市职业学院；变量2名为student_name，它的值是：张三；变量3名为student_age,它的值是：19
# 2.通过printf函数打印出： 学校名称：城市职业学院，学生姓名：张三，年龄：19。


# 二、（分数10）：类型转换
# 1.把变量str_1由字符串类型转换为浮点数类型后赋值给变量float_1
# 2.把变量int_1由整型转换为字符串类型后赋值给变量str_2
str_1 = "3.1415"

int_1 = 69



# 三、（分数10）：去除字符串变量str1的左右两端的空格，赋值给新的变量 str_4
str_3 = "   abc       "




# 四、（分数20）：使用关键字def定义一个函数，用于求两个参数的最大值，并且调用该函数
# 1.定义函数，名称为max,两个参数，名称分别为a,b
# 2.函数体里实现判断a和b的大小，通过return关键字返回最大值
# 3.调用定义好的max函数，传入参数值分别为：18,69



# 五、（分数20）：使用关键字def定义一个函数，并且判断该变量不同值输出不同内容
# 1.定义函数名：week_info，参数为week_no。函数内使用if else判断语句判断week_no的值
# 2.如果week_no == 6 or week_no == 7:  通过print打印出：优惠活动6折进行中；
#   其他值，通过print打印出：全场9.9折进行中；
# 3.调用week_info函数，传递参数值为6，使用关键字传参方式传参




# 六、（分数20）：操作列表：添加元素，遍历元素并打印
# 已有列表变量名为 phone,赋值三个元素，分别是 华为 vivo  OPPO
# 1.调用列表的添加元素函数，添加一个元素叫：小米
# 2.通过列表的读取元素方法，读取索引为1的值赋值给新的变量 phone_1
# 3.使用遍历方法，遍历列表变量phone，通过print打印出每个元素的值
phone = ["华为", "vivo", "OPPO"]




# 七、（分数80）：定义类及类里面的函数（lu_ru，da_yin，cha_zhao，zong_cheng_ji，ping_jun），以及实例化类的对象后，调用对象的函数
# 定义类（class），类名为Teacher：
# 构造函数为（可以直接把该构造函数放在类Teacher里面）:    
#     def __init__(self):
#         self.chengji = {}
# 1.函数1(函数名称为lu_ru)：支持录入学生成绩，支持一次录入多个学生（本次要求录入3个学生做例子即可），采取不定长参数两颗**的字典方式传参,函数形式为def lu_ru(self,**chengji): 把参数chengji赋值给self.chengji这个属性（该属性为字典类型）
# 2.函数2(函数名称为da_yin)：打印所有学生的成绩：在函数里遍历self.chengji，通过print打印输出里面的元素即可
# 3.函数3(函数名称为cha_zhao)：获取某个学生的成绩，参数名称叫xuesheng：使用读取字典元素值的方法，读取self.chengji里面某个键key为参数xuesheng的对应的值value
# 4.函数4(函数名称为zong_cheng_ji)：计算学生总成绩：字典的for循环实例属性self.chengji
# 5.函数5(函数名称为ping_jun)：计算学生平均成绩：计算完学生总成绩的结果，除以录入的学生数量，提示，可以在该函数直接调用函数4得到总成绩后再求平均值



# 6.实例化一个老师对象,赋值给变量名laoshi。
# 7.调用laoshi的函数lu_ru录入学生成绩：前面两个学生的成绩是 zhangsan=90,lisi=66 最后一个是考生自己的姓名拼音全称，成绩为99（比如考生叫赵六，拼音全称为zhaoliu），那么完整的传参为zhangsan=90,lisi=66,zhaoliu=99
# 8.调用laoshi的函数da_yin打印所有学生成绩：直接调用函数2即可，因为已经在函数2里面有print打印输出了。
# 9.调用laoshi的函数cha_zhao打印某个学生的成绩出来：需要通过【关键字】传参方式调用，参数xuesheng的值为考生自己的姓名拼音全称（比如考生叫赵六，拼音全称为zhaoliu）
# 10.调用laoshi的函数zong_cheng_ji计算学生总成绩:得到的函数4通过return返回的总成绩结果赋值给新变量zongchengji,然后通过print打印输出：学生总成绩为：zongchengji变量的值
# 11.调用laoshi的函数ping_jun计算学生平均成绩：得到的函数5通过return返回的平均成绩结果赋值给新变量pjcj,然后通过print打印输出：学生平均成绩为：pjcj变量的值


#【定义类及函数区】：
class Teacher():
    def __init__(self):
        self.chengji = {}
    # 补充完其他代码
    
    def lu_ru(self,**chengji):
        self.chengji = chengji



#【实例化上面的类的一个具体对象，然后调用里面的函数，并根据需要是否进行print打印输出】
laoshi = Teacher()
laoshi.lu_ru(zhangsan = 90,lisi = 66,liyong=88)

