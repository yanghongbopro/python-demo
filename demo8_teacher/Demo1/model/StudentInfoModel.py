# 定义了一个学生类
class StudentInfo():


    # 构造方法：用于在实例化类的具体对象时，初始化设置一些属性值，比如设置学生的实例属性name，age，cheng_ji
    # 【知识点】__init__构造函数的作用以及使用。
    def __init__(self,xingming,nianling):
        print("开始构造（也叫实例化）一个StudentInfoModel类的实例对象.............")
        self.name = xingming
        self.age = nianling
        # 学生的成绩属性先设为空字典，在后续调用lu_cheng_ji函数可以录入成绩
        self.cheng_ji = {}
        print("实例化StudentInfoModel类的实例对象完成，该对象的实例属性以及值为：name = %s,age=%s !!!!!!!!!!!!!!!!!!!" % (self.name,self.age))
    

    # 录成绩函数 【知识点】：demo_5.py  #10 不定长参数,两个星号 **， 参数以字典的形式传递
    def lu_cheng_ji(self,**cheng_ji):
        self.cheng_ji = cheng_ji


    # 看分数函数，【知识点】demo_4_4_字典Dict.py # 读取元素的 get方式读取字典值
    def kan_fen_shu(self,ke_cheng):
        return self.cheng_ji.get(ke_cheng)


    # 打印学生成绩函数，【知识点】demo_4_4_字典Dict.py # 遍历字典数据， 重点是
    def info(self):
        # print(type(self.cheng_ji)) # 控制台显示出dict就表示它是字典数据类型
        print("学生姓名：%s ，成绩如下：" % self.name)
        # self.cheng_ji属性变量是一个字典类型，所以可以使用字典遍历方法进行遍历处理
        if self.cheng_ji:
            for k,v in self.cheng_ji.items():
                print("课程名称：%s，分数：%d；" % (k,v))
        print("学生姓名：" + self.name + "显示完成！")
