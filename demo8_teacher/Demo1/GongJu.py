# 导入系统模块
import random

# 及格分数线 【知识点】整型变量定义和赋值
__JIGE = 60


# 考试，计算分数【知识点】普通函数定义及函数的返回
def kao_shi():
    # 使用系统模块random的函数生成一个随机数，范围在0到100
    fen_shu = random.randint(0,100)
    return fen_shu

# 判断课程分数是否及格
def shi_fou_ji_ge(ke_cheng, fen_shu):
    ji_ge = True
    if fen_shu < __JIGE:
        ji_ge = False
        print(ke_cheng+ ": 不及格得下学期重修该课程了")

    return ji_ge
