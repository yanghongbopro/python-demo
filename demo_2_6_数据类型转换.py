str_1 = '666'
str_to_int = int(str_1)
print("字符串转换为整数类型，str_to_int = %s" % str_1)

# str_2 = '666.666'
# str_to_float = float(str_2)
# print("字符串转换为浮点数类型，str_to_float = %f" % str_to_float)


# int_1 = 32123
# int_to_str = str(int_1)
# print("整型转换为字符串类型，int_to_str = %s" % int_to_str)

# int_2 = 32123
# int_to_float = float(int_2)
# print("整型转换为浮点类型，int_to_float = %f" % int_to_float)

# float_1 = 123.3212
# float_to_str = str(float_1)
# print("浮点型转换为字符串类型，float_to_str = %s" % float_to_str)
# print("浮点型，float_1 = %f" % float_1)

# float_2 = 666.666
# float_to_int = int(float_2)
# print("浮点数转换为整型，float_to_int = %d" % float_to_int)

# char_1 = 'A'
# char_to_int = ord(char_1)
# print("字符型转换为整数类型，char_to_int = %d" % char_to_int)

# char_add_int = ord('A') + 5
# print("字符A加整数5的结果，char_add_int=%d" % char_add_int)
