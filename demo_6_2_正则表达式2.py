import re

result1 = re.findall(r'\d+','runoob 123 google 456')
 
pattern = re.compile(r'\d+')   # 查找数字
result2 = pattern.findall('runoob 123 google 456')
result3 = pattern.findall('run88oob123google456', 0, 10)
print(result1)
print(result2)
print(result3)

pattern2 = re.compile(r'\s+') # 查找空格
result4 = pattern2.findall('runoob 123  google     456')
print(result4)

pattern3 = re.compile(r'[a-z]') # 查找单个小写字母
result5 = pattern3.findall('ruNoob 123 goEgle     456')
print(result5)

pattern4 = re.compile(r'[a-zA-Z]+[。|@]')  # 查找单词后面有特殊符号。和@的
result6 = pattern4.findall('ruNoob。 123 goEgle$ 456   BaIdU@')
print(result6)


pattern5 = re.compile(r'([a-zA-Z]+)[。|@]')# 查找单词后面有特殊符号。和@的,但不包含。和@这特殊符号
result7 = pattern5.findall('ruNoob。 123 goEgle$ 456   BaIdU@')
print(result7)

pattern6 = re.compile(r'src=([a-zA-Z0-9]+)\.{1}(jpg+)') # 查找
result8 = pattern6.findall('src=meinv00..jpg$#src=meinv001.jpg 123 src=meinv002.afasfa456&*BaIdU@src=meinv005.jpgasf')
for item in result8:
    print(item[0])

pattern7 = re.compile(r'src=([a-zA-Z0-9]+\.{1}jpg+)') # 查找
result9 = pattern7.findall('sadf#sdgfsrc=meinv00..jpg$#src=meinv001.jpg 123 src=meinv002.fgfsfa456&*BaIdU@src=meinv005.jpgasf')
for item in result9:
    print(item)



html_content = ' "isNeedAsyncRequest":0,"data":[{"thumbURL":"https://img2.baidu.com/it/u=1835843610,1575206394&fm=253&fmt=auto&app=120&f=JPEG?w=500&h=999","middleURL":"https://img2.baidu.com/it/u=1835843610,1575206394&fm=253&fmt=auto&app=120&f=JPEG?w=500&h=999",            "largeTnImageUrl":"",            "hasLarge" :0,            "hoverURL":"https://img2.baidu.com/it/u=1835843610,1575206394&fm=253&fmt=auto&app=120&f=JPEG?w=500&h=999",            "pageNum":0,            "objURL":"https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fblog%2F202109%2F06%2F20210906225922_1c31b.thumb.1000_0.jpeg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1670472716&t=d12342347ee3b8e9231839c5a662d0fa",            "fromURL":"ippr_z2C$qAzdH3FAzdH3Fooo_z&e3B17tpwg2_z&e3Bv54AzdH3Fks52AzdH3F?t1=8nm90b9dc9",            "fromURLHost":"www.duitang.com",            "currentIndex":"",            "width":800,            "height":1598,            "type":"jpeg",            "filesize":"",            "bdSrcType":"0",            "di":"7146857200093233153",            "pi":"0",            "is":"0,0",            "partnerId":0,            "bdSetImgNum":0,            "bdImgnewsDate":"2021-09-06 22:59",            "hoverURL":"https://img0.baidu.com/it/u=3375911127,635571288&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889",            "pageNum":1,"fromPageTitle":"美女壁纸 - 堆糖,美图壁纸兴趣社区",            "bdSourceName":""'
result = re.findall('"hoverURL":"(.*?)"', html_content)

for item in result:
    print(item)