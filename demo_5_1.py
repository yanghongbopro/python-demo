#!/usr/bin/env python
# encoding: utf-8
'''
@author: burette
@file: 5.1.py
@desc:
'''


def filterchar(string):
    """
    功能：过滤危险字符，并将过滤后的结果输出
    :param string:
    :return:
    """
    import re
    pattern = r'(黑客)|(抓包)|(监听)'  # 模式字符串
    sub = re.sub(pattern, "@_@", string)  # 进行模式替换
    print(sub)


about = "小明喜欢看黑客相关的图书，擅长于网络抓包"
filterchar(about)

# 定义函数
# def demo(obj):
#     print("原值：", obj)
#     obj += obj
#
#
# 调用函数
# print("=========值传递========")
# mot = "唯有在被追赶的时候,你才能真正地奔跑。"
# print("函数调用前:", mot)
# demo(mot)  # 采用不可变对象一字符串
# print("函数调用后:", mot)
# print("=========引用传递========")
# list1 = ['绮梦', '冷伊一', '香凝', '黛兰']
# print("函数调用前:", list1)
# demo(list1)  # 采用可变对象一列表
# print("函数调用后:", list1)


def demo(obj=None):  # 定义函数并为obj指定默认值
    if obj == None:
        obj = []
    print("obj的值：", obj)
    obj.append(1)


# demo()

def printcoffee(*coffername):  # 定义输出我喜欢的咖啡名称的函数
    print("\n我喜欢的咖啡有：")
    for item in coffername:
        print(item)  # 输出咖啡名称


# param = ['蓝山', '卡布奇诺', '土耳其']
# printcoffee(*param)

# printcoffee('蓝山')
# printcoffee('蓝山', '卡布奇诺', '土耳其', '巴西', '哥伦比亚')
# printcoffee('蓝山', '卡布奇诺', '曼特宁', "摩卡")


def printsign(**sign):
    print()
    for key, value in sign.items():
        print("[" + key + "] 的星座是：" + value)


# 调用函数
# printsign(绮梦='水瓶座', 冷伊一='射手座')
# printsign(香凝擬='双鱼座', 黛兰='双子座', 冷伊一='射手座')





#!/usr/bin/env python
# encoding: utf-8
'''
@author: burette
@file: checkout.py
@desc:
'''


def fun_checkout(money):
    """
    功能:计算商品合计金额并进行折扣处理
    money:保存商品金额的列表
    返回商品的合计金额和折扣后的金额
    :param money:
    :return:
    """
    money_old = sum(money)
    money_new = money_old
    if 500 <= money_old < 1000:
        money_new = '{:.2f}'.format(money_old * 0.9)
    elif 1000 <= money_old <= 2000:
        money_new = '{:.2f}'.format(money_old * 0.8)
    elif 2000 <= money_old <= 3000:
        money_new = '{:.2f}'.format(money_old * 0.7)
    elif money_old >= 3000:
        money_new = '{:.2f}'.format(money_old * 0.6)
    return money_old, money_new  # 返回总金额和折扣后的金额


# 调用函数
print("\n开始结算......\n")
list_money = []
while True:
    # 请不要输入非法的金额,否则将抛出异常
    inmoney = float(input("输入商品金额(输入0表示输入完毕):"))
    if int(inmoney) == 0:
        break
    else:
        list_money.append(inmoney)
money = fun_checkout(list_money)
print("合计金额:", money[0], "应付金额:", money[1])



#!/usr/bin/env python
# encoding: utf-8
'''
@author: burette
@file: function_bmi_upgrade.py
@desc:
'''


def fun_bmi_upgrade(*person):
    """
    功能:根据身高和体重计算BMI指数（共享升级版）
    *person:可变参数该参数中需要传递带3个元素的列表,
    分别为姓名、身高（单位:米）和体重（单位:千克）
    :param person:
    :return:
    """
    for list_person in person:
        for item in list_person:
            person = item[0]
            height = item[1]
            weight = item[2]
            print("\n" + "=" * 13, person, "=" * 13)
            print("身高：" + str(height) + "米\t 体重：" + str(weight) + "千克")
            bmi = weight / (height * height)  # 用于计算BMI指数,公式为: BMI=体重/身高的平方
            print("BMI指数:" + str(bmi))  # 输出BMI指数
            # 判断身材是否合理
            if bmi < 18.5:
                print("您的体重过轻~@_@")
            if bmi >= 18.5 and bmi < 24.9:
                print("正常范围,注意保持(-_-)")
            if bmi >= 24.9 and bmi < 29.9:
                print("您的体重过重 ~@_@")
            if bmi >= 29.9:
                print("肥胖^@_@^")


# 调用函数
list_w = [('绮梦', 1.70, 65), ('零语', 1.78, 50), ('黛兰', 1.72, 66)]
list_m = [('梓轩', 1.80, 75), ('冷伊一', 1.75, 70)]
fun_bmi_upgrade(list_w, list_m)  # 调用函数指定可变参数



#!/usr/bin/env python
# encoding: utf-8
'''
@author: burette
@file: function_bmi.py
@desc:
'''


def fun_bmi(height, weight, person="路人"):
    """
    功能：根据身高和体重计算BMI指数
    :param person:姓名
    :param height:身高
    :param weight:体重
    :return:
    """
    bmi = weight / (height * height)
    print(person + "的BMI指数为：" + str(bmi))
    # 判断身材是否合理
    if bmi < 18.5:
        print("您的体重过轻~@_@~\n")
    if bmi >= 18.5 and bmi < 24.9:
        print("正常范围,注意保持(-_-) \n")
    if bmi >= 24.9 and bmi < 29.9:
        print("您的体重过重 ~@_@\n")


# 调用函数
# fun_bmi(height=1.83, weight=60, person="路人甲")  # 计算路人甲的BMI指数
# fun_bmi("路人甲", 60, 1.83)  # 计算路人甲的BMI指数
fun_bmi(1.73,60)

exit(0)
fun_bmi(60, "路人甲", 1.83)  # 计算路人甲的BMI指数
fun_bmi("路人甲", 1.83)  # 计算路人甲的BMI指数
fun_bmi("路人甲", 1.83, 60)  # 计算路人甲的BMI指数
fun_bmi("路人乙", 1.60, 50)  # 计算路人乙的BMI指数


#!/usr/bin/env python
# encoding: utf-8
'''
@author: burette
@file: function_tips.py
@desc:
'''


def function_tips():
    """
    功能:每天输出一条励志文字
    :return:
    """
    import datetime  # 导入日期时间类
    # 定义一个列表
    mot = ["今天星期一:\n坚持下去不是因为我很坚强,而是因为我别无选择",
           "今天星期二:\n含泪播种的人一定能笑着收获",
           "今天星期三:\n做对的事情比把事情做对重要",
           "今天星期四:\n命运给予我们的不是失望之酒,而是机会之杯",
           "今天星期五:\n不要等到明天,明天太遥远,今天就行动",
           "今天星期六:\n求知若饥,虚心若愚",
           "今天星期日:\n成功将属于那些从不说“不可能”的人"]
    # 获取当前星期
    day = datetime.datetime.now().weekday()
    print(mot[day])


#  调用函数
function_tips()




#!/usr/bin/env python
# encoding: utf-8
'''
@author: burette
@file: seckillsort.py
@desc:
'''

bookinfo = [('不一样的卡梅拉(全套)', 22.50, 120), ('零基础学Android', 65.10, 89.80),
            ('摆渡人', 23.40, 36.00), ('福尔摩斯探案全集8册', 22.50, 128)]
print("爬取到到商品信息：\n", bookinfo, "\n")
bookinfo.sort(key=lambda x: (x[1], x[1] / x[2]))
print("排序后到商品信息：\n", bookinfo)
