""" 
基类，被别的类继承用。定义了公共通用的
类属性：yan_jing，bi_zi;
实例对象属性：xing_ming,nian_ling;
方法(就是函数)：chi_fan,shui_jiao（这个方法被@property改为属性）
"""
class RenLei():
    """人类模板类"""
    # 类属性：yan_jing，bi_zi，其他地方获取这两个属性变量的值的调用方式是：RenLei.yan_jing,RenLei.bi_zi
    yan_jing = 2
    bi_zi = 1

    # 初始化对象方法，创建一个该类（RenLei）模板下一个具体的对象东西，比如调用方式：zs = RenLei("张三",18)，其中("张三",18)这两个参数是传递给__init__这个方法的，然后调用该方法
    def __init__(self,name,age):
        # 实例属性：xing_ming,nian_ling;其他地方获取这两个属性变量的值的调用方式是：实例变量名称.xing_ming,比如上面的zs.xing_ming的值是 张三
        self.xing_ming = name
        self.nian_ling = age
        print("【RenLei】创造一个人完毕，姓名：%s，年龄:%d，他有：%d个眼睛，%d个鼻子。" % (self.xing_ming,self.nian_ling,RenLei.yan_jing,RenLei.bi_zi))
    
    # 实例方法：调用方法比如上面创建的zs，调用为：zs.chi_fan("榴莲披萨")
    def chi_fan(self,chi_de_shi_sha):
        print("【RenLei】%s吃了%s\n" % (self.xing_ming, chi_de_shi_sha))
    
    # 实例方法被转为实例属性：调用方法比如上面创建的zs，调用为：zs.shui_jiao，注意，属性调用后面没有小括号，而方法需要，这里已经是属性了，所以调用时不需要加小括号
    @property
    def shui_jiao(self):
        print("【RenLei】%s睡觉了\n" % (self.xing_ming))




"""
派生类，它继承了上面的人类RenLei（基类）。
它继承了RenLei的所有东西，比如：
类属性：yan_jing，bi_zi;
实例对象属性：xing_ming,nian_ling;
方法(就是函数)：chi_fan,shui_jiao（这个方法被@property改为属性）


然后加了自己的内容：
类属性：pi_fu，__pao_cai_mi_fang（该属性前面有两个下划线，变为私有的，即类外部不能直接调用访问它，只能在实例方法里访问）;

方法(就是函数)：
重写了基类（RenLei）的chi_fan方法，在基类的chi_fan方法前后加了额外的业务逻辑
增加一个新方法：kan_kan_pao_cai_mi_fang：看看泡菜秘方，用于把私有的类属性给调用方，当然加了一些特殊处理，比如替换了里面的49。

"""
class HanGuoRen(RenLei):
    """航国人类模板"""
    # HanGuoRen额外加的类属性：pi_fu，__pao_cai_mi_fang（该属性前面有两个下划线，变为私有的，即类外部不能直接调用访问它，只能在实例方法里访问）
    # 其他地方获取pi_fu属性变量的值的调用方式是：HanGuoRen.pi_fu
    pi_fu = "黄色"
    # 其他地方获取__pao_cai_mi_fang属性变量的值的调用方式是：HanGuoRen.__pao_cai_mi_fang会报错，实例方法里面调用不会报错
    __pao_cai_mi_fang = "白菜加绝密辣椒泡49天"

    def __init__(self, hgr_name, hgr_age):
        # super()为上级对象的意思，也就是继承的基类RenLei的实例对象。super().__init__()是调用了基类RenLei的方法
        super().__init__(hgr_name, hgr_age)

    # 重写了基类（RenLei）的chi_fan方法，在基类的chi_fan方法前后加了额外的业务逻辑
    def chi_fan(self, chi_de_shi_sha):
        # 在下面一行调用基类的chi_fan方法之前加一个业务代码
        print("【HanGuoRen】吃饭前-%s 餐前加了一碟泡菜\n" % (self.xing_ming))
        # 调用基类（RenLei）的chi_fan方法
        super().chi_fan(chi_de_shi_sha)
        # 在上面面一行调用基类的chi_fan方法之后加一个业务代码
        if(self.nian_ling > 50):
            print("【HanGuoRen】吃饭后- %s他今年%d岁，超过了50，喝了一碗人参乌鸡汤\n" %(self.xing_ming, self.nian_ling))

    # 增加一个新方法：kan_kan_pao_cai_mi_fang：看看泡菜秘方，用于把私有的类属性给调用方，当然加了一些特殊处理，比如替换了里面的49。
    def kan_kan_pao_cai_mi_fang(self):
        jia_mi_pao_cai_mi_fang = HanGuoRen.__pao_cai_mi_fang.replace("49","【商业秘密】")
        return jia_mi_pao_cai_mi_fang





"""
派生类，它继承了上面的人类RenLei（基类）。
它继承了RenLei的所有东西，比如：
类属性：yan_jing，bi_zi;
实例对象属性：xing_ming,nian_ling;
方法(就是函数)：chi_fan,shui_jiao（这个方法被@property改为属性）


然后加了自己的内容：
类属性：pi_fu，shi_wu_cai_dan;

实例对象属性__shi_wu，该实例属性为私有，外面的实例对象是无法直接调用获取该实例属性的值！

方法(就是函数)：
重写了基类（RenLei）的__init__方法，在基类的__init__方法后加了额外的业务逻辑
重写了基类（RenLei）的chi_fan方法，在基类的chi_fan方法前后加了额外的业务逻辑
增加了一个属性（方法被 @property转换的）：shi_wu
增加了一个属性设置值的方法：shi_wu，在该属性赋值加了一些代码处理。
"""
class YinDuRen(RenLei):
    """银都人类模板"""

    # 类属性：pi_fu，其他地方获取属性变量的值的调用方式是：YinDuRen.pi_fu
    pi_fu = "比黄颜色再深一点点"
    # 类属性：shi_wu_cai_dan，其他地方获取属性变量的值的调用方式是：YinDuRen.shi_wu_cai_dan
    shi_wu_cai_dan = ["咖喱鸡肉","咖喱鱼头","咖喱牛腩"]

    #重写了基类（RenLei）的__init__方法，在基类的__init__方法后加了额外的业务逻辑
    def __init__(self, name, age, xi_huan_chi = "咖喱鱼头"):
        # 调用基类（RenLei）的__init__方法
        super().__init__(name, age)
        # 然后加了YinDuRen额外的业务逻辑
        self.__shi_wu = xi_huan_chi
        print("皮肤：%s\n" % YinDuRen.pi_fu)

    #重写了基类（RenLei）的chi_fan方法，在基类的chi_fan方法后加了额外的业务逻辑
    def chi_fan(self, chi_de_shi_sha):
        #先调用基类（RenLei）的chi_fan方法
        super().chi_fan(chi_de_shi_sha)
        #在基类的chi_fan方法后加了额外的业务逻辑
        print("%s说：干净又卫生\n" % self.xing_ming)

    # 增加一个实例属性，由普通方法转为属性的，用了@property实现这个效果
    @property
    def shi_wu(self):
        # 访问了私有实例属性，因为外面无法直接访问
        return self.__shi_wu

    # 增加了一个属性设置值的方法：shi_wu，用于设置私有实例属性的值，因为外面无法直接调用，所以通过这种方式可以实现
    @shi_wu.setter
    def shi_wu(self,value):
        if value in YinDuRen.shi_wu_cai_dan:
            print("您点了《" + value + "》，稍后上菜")
            self.__shi_wu = value
        else:
            print("您点的菜没有")
            self.__shi_wu = "您点的菜没有"
