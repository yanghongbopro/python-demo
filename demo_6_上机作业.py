# 【作业题1】：[考点：字符串拼接] 把这三个变量拼接起来，并且打印到控制台终端
str1 = '张三今天吃了'  # 定义字符串
num = 3  # 定义一个整数
str1_2 = "碗饭"  # 定义字符串
# 请在下面写出答案







# 【作业题2】：[考点：字符串截取] 截取变量str2的字符串内容，取第2个到第6个字符，并且打印出来
str2='人生苦短，我用Python!'  #定义字符串
# 请在下面写出答案







# 【作业题3】：[考点：字符串分割] 通过特殊符号将下面字符串str3分割为三个元素，形成列表变量，再通过列表的遍历方法打印出每个元素的值
str3 = "张三,李四,王五"
# 请在下面写出答案





# 【作业题4】：[考点：检索字符串 find] 打印以下str1变量的内容里的 @ 字符首次出现的位置是多少
str4 = '张三@百度@李彦宏@麻花藤'
# 请在下面写出答案





# 【作业题5】：[考点：去除特殊符号] 把字符串变量str5的前后空格去除，然后再去除右边的所有#符号，左边的#符号不能去除！并且把最终的字符串打印出来
str5 = "  #abc####    "
# 请在下面写出答案

