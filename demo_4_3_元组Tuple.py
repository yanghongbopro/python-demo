
# 【元组】()
# 声明并赋值元组
untitle = ('Python', 28, ("人生苦短", "我用Python"), ["爬虫", "自动化运维", "云计算", "Web开发"])
print(untitle)

ukguzheng = "渔舟唱晚", "高山流水", "出水莲", "汉宫秋月"
print(type(ukguzheng))
print(ukguzheng)

# 访问元组元素
print(untitle[0])
print(untitle[1:3])

# 修改元组：元组是不可变序列，只能重新赋值。
coffeename = ('蓝山', '卡布奇诺', '曼特宁', '摩卡', '麝香猫', '哥伦比亚')  # 定义元组
coffeename = ('蓝山1', '卡布奇诺', '曼特宁', '摩卡', '拿铁', '哥伦比亚')  # 对元组进行重新赋值
print("新元组", coffeename)

# 遍历元组并打印里面的元素
for item in coffeename:
    print(item)


#【元组推导】
import random

randomnumber = (random.randint(10, 100) for i in range(10))
print("转换前：", randomnumber)
randomnumber_2 = tuple(randomnumber)
print("转换后：", randomnumber_2)
