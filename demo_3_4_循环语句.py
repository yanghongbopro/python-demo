
# 示例1 while语句循环 while True 
i = 0
result_1 = i < 10
while result_1:
    print('i = %d' % i)
    i = i + 1
    result_1 = i < 10
    
# 示例1 -----------------------------------

# 示例2
print("计算1 + 2 + 3 + ... +100 的结果为：")
result_2 = 0
for i  in range(101):
    result_2 += i
print('result_2的计算结果是: %d ' % result_2)
# 示例2 -----------------------------------

# 示例3 奇数偶数算法
print("输出 1-10的奇数")
for i  in range(1,11,2):
    print("i = %d " % i)

print("输出 1-10的偶数")
for i in range(1, 10):
    if i % 2 == 0:  # 判断是否为偶数
        print(i, end=' ')
    else:  # 不是偶数
        pass  # 占位符，不做任何事
print("\n")
# 示例3 -----------------------------------


# 示例4
string = '我相信我可以'
print(string)  # 横向显示
for ch in string:
    print(ch)  # 纵向显示
# 示例4 -----------------------------------

# 示例5 嵌套循环 打印乘法表
print("\n以下是乘法表输出。。。。。。")
for i in range(1, 10):
    for j in range(1, i + 1):
        result_5 = i * j
        print_value = "%d * %d = %d\t" %(i, j, result_5)
        print(print_value, end="")
    print("")
