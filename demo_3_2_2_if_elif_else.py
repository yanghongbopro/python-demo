# # 示例1 if elif else的用法
hour = int(input("请输入现在是几点："))

if hour >= 0 and hour < 11:
    print("上午好\n")
elif hour >=11 and hour < 13:
    print("中午好\n")
elif hour >= 13 and hour < 18:
    print("下午好")
else:
    print("晚上好")
# # 示例1 ----------------------------------------------------------------------

# 示例1 if语句嵌套 
print("超市活动进行中。。。")
int_week = input("请输入星期几（如星期一则输入1）：")
int_time = int(input("请输入时间中的小时（范围：0 ~ 24）："))

if int_week == 6 or int_week == 7:
    if int_time >= 9 and int_time <= 18:
        print("周末白天优惠活动进行中~\n")
    else:
        print("优惠活动尚未开始~\n")

else:
    if int_time >= 20 and int_time <= 24:
        print("晚上清仓优惠活动进行中~\n")
    else:
        print("晚上清仓优惠活动尚未开始~\n")