
week_no = "星期六"
if week_no == "星期六" or week_no == "星期日":
    # 代码缩进，冒号后面的代码缩进，4个空格，或者一个tab，或者回车后编辑器自动帮你缩进好，直接写下面代码即可
    print("又到周末了，没得课上，哎")

else:
    # 代码缩进，冒号后面的代码缩进，4个空格，或者一个tab，或者回车后编辑器自动帮你缩进好，直接写下面代码即可
    print("又到上课的好日子？")

student_name = '张三'
print("学生姓名：%s" % (student_name))


#定义函数max，记住定义函数没被其他地方调用，里面的代码是不会被执行的，直到有地方调用，比如max(11,9) 得到结果：11
def max(a, b):
    if a > b:
        return a
    else:
        return b



