# 示例1 break，直接终止剩余的执行
i = 0
while i < 10:
    print("开始：i = %d" % i)
    
    if i == 6:
        print("终止执行，后面的循环(i=7,8,9)不再执行了")
        break
    else:
        # print("还没到终止执行条件")
        pass # 3.6 知识点，pass空语句，该语句不做任何事情
        
    i+=1   
    print("继续执行下一次吧，下一次循环时 i = %d" % i)



j = 0
while j < 10:
    print("开始：j = %d" % j)
    if j == 6:
        print("终止本次循环，即本次循环的后续代码不再执行")
        j+=1
        continue
    else:
        # print("还没到终止执行条件")
        pass
    j+=1 
    print("继续执行下一次吧，下一次循环时 j = %d" % j) # 注意，当j等于6时，这句代码是没有执行到的，直接在continue的时候直接到while的新一轮循环了，也就是开始j=7及后面的运算了