
# 示例1   
result_1 = False
if result_1:
    print("result_1是真")
else:
    print("result_1是假")
# 示例1 ----------------------------------------------------------------------

# 示例2
result_2 = None
if result_2:
    print("None是真")
else:
    print("None是假")
# 示例2 ----------------------------------------------------------------------

# 示例3
result_3 = []
if result_3:
    print("[]是真")
else:
    print("[]是假")
# 示例3 ----------------------------------------------------------------------

    
# 示例4
result_4 = 0.0
if result_4:
    print("0.0是真")
else:
    print("0.0是假")
# 示例4 ----------------------------------------------------------------------


# 示例5
result_5 = 0.0
if result_5:
    print("0.0是真")
else:
    print("0.0是假")
# 示例5 ----------------------------------------------------------------------


# 示例6
a = 3
b = 2 
if a > b:
    print("3大于2是真\n")
else:
    print("3大于2假\n")
# 示例6 ----------------------------------------------------------------------


# 示例7 求绝对值
a = -9
b = None
if a > 0:
    b = a
else:
    b = -a

print("b = %d\n" % b)
# 示例7 ----------------------------------------------------------------------

# 示例8 求绝对值  等同示例7，相当于简化语法
c = -9
d = None
# 条件表达式 书本3.3的知识点
d = c if c > 0 else -c

print("d = %d\n" % d)
# 示例8 ----------------------------------------------------------------------