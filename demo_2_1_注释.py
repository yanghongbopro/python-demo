
# 单行注释用法：在终端打印输出：hello world! (注意：# 后面一整行文字都是注释，即不是真正的可运行的代码，注释常用于说明解析对应的代码)
print("hello world!")

"""
多行注释用法1：使用三个双引号包住
注释内容:
注释内容
"""
print("演示多行注释用法,使用两个\"\"\"包住！")


'''
多行注释用法2：使用三个单引号包住
注释内容1
注释内容2
'''
print("演示多行注释用法,使用两个'''包住！")

# 【重要！！！】注释的快捷键(两个一起按)：ctrl + /   重复按切换注释和不注释