
# # 纯数字列表 
num = [1, 4, 2, 7, 13, 43, 25, 8, 10]
# # 纯字符串
# string = ["渔舟唱晚", "高山流水", "出水莲", "汉宫秋月"]
# # 大杂烩
# untitle = ['Python', 28, ("人生苦短", "我用Python"), ["爬虫", "自动化运维", "云计算", "Web开发"]]
# # 空列表
# empty_list = []

# # 通过list转range数字为列表
# range_list = list(range(10,20,2))
# print(range_list)


# # 访问列表
# print(type(untitle))
# print(untitle)
# print(untitle[0]) # 通过下标索引访问列表里面元素
# print(untitle[2]) # 通过下标索引访问列表里面元素
# print(untitle[len(untitle)-1]) # 通过下标索引访问列表里面元素

# # 【实例01】输出每日一贴
# import datetime  # 导入日期时间类
# # 定义一个列表
# mot = ["今天星期一：\n坚持下去不是因为我很坚强，而是因为我别无选择。"
#        "今天星期二：\n含泪播种的人一定能笑着收获。",
#        "今天星期三：\n做对的事情比把事情做对重要。",
#        "今天星期四：\n命运给予我们的不是失望之酒，而是机会之杯。",
#        "今天星期五：\n不要等到明天，明天太遥远，今天就行动。",
#        "今天星期六：\n求知若饥，虚心若愚。",
#        "今天星期日：\n成功将属于那些从不说“不可能”的人。"]
# day = datetime.datetime.now().weekday()
# print(mot[day])


# 遍历列表 for in 
xushengs = ["zhangsan", "lisi", "wangwu"]
for a in xushengs:
    print(a)

# for index, item in enumerate(team):
#     print(index + 1, item)

# # 添加元素
phone = ["摩托罗拉", "诺基亚", "三星", "OPPO"]
print(phone)
print(len(phone)) # 获取列表的长度
  
phone.append("iPhone")
print(phone)
print(len(phone)) # 获取列表的长度

# # 列表加列表
# a = list(range(1,10,2))
# b = list(range(2,11,2))
# print(a)
# print(b)
# a = a.extend(b)
# print(a)
# print(b)

# # 修改列表元素
phone_2 = ["摩托罗拉2", "vivo2", "三星2", "OPPO2"]
# print(phone_2)
phone_2[1] = 'vivo2'
# print(phone_2)

# # 删除列表元素
phone_3 = ["摩托罗拉3", "诺基亚3", "三星3", "OPPO3"]
print(phone_3)
del phone_3[2]
print(phone_3)


# # 删除列表元素
# phone_4 = ["摩托罗拉4", "诺基亚4", "三星4", "OPPO4"]
# print(phone_4)
# value = "诺基亚42"
# if phone_4.count(value) > 0:
#     phone_4.remove(value)
#     print(phone_4)

# # 获取指定元素首次出现下标
# phone_5 = ["摩托罗拉5", "诺基亚5", "三星5", "OPPO5", "诺基亚5", "诺基亚5"]
# print(phone_5)
# value = "诺基亚5"
# position = phone_5.index(value)
# print("%s下标：%d\n" % (value,position))



# # 【列表排序-列表自身的sort方法】
# grade = [98, 99, 97, 100, 100, 96, 97, 89, 95, 100]  # 10名学生语文成绩列表
# print(id(grade))
# print("原列表：", grade)
# grade.sort()  # 进行升序排列
# print("升序：", grade)
# grade.sort(reverse=True)  # 进行降序排列
# print("降序:", grade)
# print("原序列:", grade)
# print(id(grade))

# # 【列表排序-sorted内置函数】
# grade2 = [98, 99, 97, 100, 100, 96, 97, 89, 95, 100]  # 10名学生语文成绩列表
# print(id(grade2))
# grade_as = sorted(grade2)  # 进行升序排列
# print("升序:", grade_as)
# grade_des = sorted(grade, reverse=True)  # 进行降序排列
# print("降序:", grade_des)
# print("原序列:", grade2)
# print(id(grade_des))


# # 列表排序-区分大小写
# char = ['cat', 'Tom', 'Angela', 'pet']
# char.sort()  # 默认区分字母大小写
# print("区分字母大小写：", char)
# char.sort(key=str.lower)  # 不区分字母大小写
# print("不区分字母大小写：", char)


# #【range推导列表】
# import random  # 导入random标准库

# randomnumber = [random.randint(10, 100) for i in range(10)]
# print("生成对随机数为：", randomnumber)


# 列表推导 
price = [80, 20, 100, 30, 55, 112, 115,34,22,100]
yh_price = [item for item in price if item >= 100]
yh_price_aft = [int(item * 0.5) for item in price if item >= 100]
print("超过100的价格：",yh_price)
print("超过100打5折后的价格：",yh_price_aft)

# #【二维列表】
# arr = []
# for i in range(4):
#     arr.append([])
#     for j in range(5):
#         arr[i].append(j)

# print(arr)





