# 基础语法

# 第一个注释
print ("Hello, Python!") # 第二个注释

# 行与缩进
# python最具特色的就是使用缩进来表示代码块，不需要使用大括号 {} 。

# 缩进的空格数是可变的，但是同一个代码块的语句必须包含相同的缩进空格数。实例如下：


# 【标识符】作用：给变量，函数，参数等起名字，也就是命名
# 规则：
# 第一个字符必须是字母表中字母或下划线 _ 。
# 标识符的其他的部分由字母、数字和下划线组成。
# 不合法命名： 6_lao  name$
# 合法的命名：
# user_name
# user_age
# list_2

# 举例: 定义学生信息及三门课程的分数
# 定义变量如下
user_name = "张三"
user_age = 18
user_class = "网络班"
python_score = 80.5
java_score = 90
linux_score = 75

# 定义计算学生总分数函数
def count_student_total_score(user_name, user_age, user_class, python_score, java_score, linux_score):
    total_score = python_score + java_score + linux_score
    result = "学生姓名：%s，年龄：%d，班级：%s，总分：%.2f。\n" % (user_name, user_age, user_class,total_score)
    return result

# 调用函数count_student_total_score得到结果，然后调用print函数打印输出结果内容
student_total_score = count_student_total_score(user_name, user_age, user_class, python_score, java_score, linux_score)
print(student_total_score)


# 【python保留字】
# 保留字即关键字，我们不能把它们用作任何标识符名称。Python 的标准库提供了一个 keyword 模块，可以输出当前版本的所有关键字：



# 字符串
str='123456789'

print(str)                 # 输出字符串
print(str[0:-1])           # 输出第一个到倒数第二个的所有字符
print(str[0])              # 输出字符串第一个字符
print(str[2:5])            # 输出从第三个开始到第五个的字符
print(str[2:])             # 输出从第三个开始后的所有字符
print(str[1:5:2])          # 输出从第二个开始到第五个且每隔一个的字符（步长为2）
print(str * 2)             # 输出字符串两次
print(str + '你好')         # 连接字符串

